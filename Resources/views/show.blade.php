@extends('neev::layouts.app')
@include('themestylist::layouts.master')

@section('content')
    <h1>
        <small>
            <a href="{{ route('listthemes') }}" data-toggle="tooltip"
               title="" data-original-title="Back">
                <i class="fa fa-reply"></i>
            </a>
        </small>
        {{ $theme->getName() }} <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('user.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('listthemes') }}">Themes</a></li>
        <li class="active">{{ $theme->getName() }}</li>
    </ol>

@push('css-stack')
    <style>
        .module-type {
            text-align: center;
        }
        .module-type span {
            display: block;
        }
        .module-type i {
            font-size: 124px;
            margin-right: 20px;
        }
        .module-type span {
            margin-left: -20px;
        }
        form {
            display: inline;
        }
    </style>
@endpush

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool jsPublishAssets" data-toggle="tooltip"
                                title="" data-original-title="Publish Assets">
                            <i class="fa fa-cloud-upload"></i>
                            Publish Assets
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6 module-details">
                            <div class="module-type pull-left">
                                <i class="fa fa-picture-o"></i>
                                <span>
                                    {{  $theme->version }}
                                </span>
                            </div>
                            <h2>{{ ucfirst($theme->getName()) }}</h2>
                            <p>{{ $theme->getDescription() }}</p>
                        </div>
                        <div class="col-sm-6">
                            <dl class="dl-horizontal">
                                <dt>Type :</dt>
                                <dd>{{ $theme->type }}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if (!empty($theme->changelog) && count($theme->changelog['versions'])): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-bars"></i> ChangeLog</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    @include('themestylist::partials.changelog', [
                        'changelog' => $theme->changelog
                    ])
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <script>

@stop

@push('js-stack')
$( document ).ready(function() {
    $('.jsPublishAssets').on('click',function (event) {
        event.preventDefault();
        var $self = $(this);
        $self.find('i').toggleClass('fa-cloud-upload fa-refresh fa-spin');
        $.ajax({
            type: 'POST',
            url: '{{ route('publishAssets', [$theme->getName()]) }}',
            data: {_token: '{{ csrf_token() }}'},
            success: function() {
                $self.find('i').toggleClass('fa-cloud-upload fa-refresh fa-spin');
            }
        });
    });
});
</script>
@endpush
