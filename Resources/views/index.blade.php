@extends('neev::layouts.app')
@include('themestylist::layouts.master')

@section('content')
<h1>
   Theme
</h1>
<ol class="breadcrumb">

</ol>


@push('css-stack')
    <style>
        .jsUpdateModule {
            transition: all .5s ease-in-out;
        }
    </style>
@endpush
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="data-table table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th width="15%">Type</th>
                        <th width="15%">Version</th>
                        <th width="15%">Enabled</th>
                        <th width="15%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($themes)): ?>
                        <?php foreach ($themes as $theme): ?>
                            <tr>
                                <td>
                                    <a href="{{ route('details', [$theme->getName()]) }}">
                                        {{ $theme->getName() }}
                                    </a>
                                </td>
                                <td>

                                        {{ $theme->type }}
                                    </a>
                                </td>
                                <td>

                                        {{ $theme->version }}

                                </td>
                                <td>
                                    <span class="label label-{{$theme->active ? 'success' : 'danger'}}">
                                        {{ $theme->active ? 'Enabled' : 'Disabled' }}
                                    </span>
                                </td>
                                <td>
                                    <?php if ($theme->active == false): ?>
                                    <form  method="post" action="{{route('update', [$theme->getname(), 'type' => $theme->type])}}" >
                                            {{ csrf_field() }}
                                        <button type="submit" class="btn btn-primary btn-xs">Activate</button>
                                    </form>
                                <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                    <tfoot>
                    <tr>

                    </tr>
                    </tfoot>
                </table>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
@stop
@push('js-stack')

    <script>
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "asc" ]],,
                "columns": [
                    null,
                    null,
                    null,
                    null,
                ]
            });
        });
    </script>
<script>
$( document ).ready(function() {
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
    $('.jsUpdateModule').on('click', function(e) {
        $(this).data('loading-text', '<i class="fa fa-spinner fa-spin"></i> Loading ...');
        var $btn = $(this).button('loading');
        var token = '<?= csrf_token() ?>';
        $.ajax({
            type: 'POST',
            url: '',
            data: {module: $btn.data('module'), _token: token},
            success: function(data) {
                console.log(data);
                if (data.updated) {
                    $btn.button('reset');
                    $btn.removeClass('btn-primary');
                    $btn.addClass('btn-success');
                    $btn.html('<i class="fa fa-check"></i> Module updated!')
                    setTimeout(function() {
                        $btn.removeClass('btn-success');
                        $btn.addClass('btn-primary');
                        $btn.html('Update')
                    }, 2000);
                }
            }
        });
    });
});
</script>
@endpush

