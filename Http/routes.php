<?php

Route::group(['middleware' => 'web', 'prefix' => 'webthemes', 'namespace' => 'Modules\Themestylist\Http\Controllers'], function()
{
    Route::get('/list', 'ThemestylistController@index')->name('listthemes');
    Route::get('/{theme}', 'ThemestylistController@show')->name('details');
    Route::post('/{theme}/publish', 'ThemestylistController@publishAssets')->name('publishAssets');
    Route::post('/{theme}/update', 'ThemestylistController@store')->name('update');

});
