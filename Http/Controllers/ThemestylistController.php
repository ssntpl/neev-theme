<?php

namespace Modules\Themestylist\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use FloatingPoint\Stylist\Theme\Theme;
use FloatingPoint\Stylist\Theme\Exceptions\ThemeNotFoundException;
use FloatingPoint\Stylist\Theme\Json;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Illuminate\Support\Facades\Redirect;


class ThemestylistController extends Controller
{

     /**
     * @var Filesystem
     */
    private $finder;

    public function __construct(Filesystem $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $directories = $this->getDirectories();
        $themes = [];
        foreach ($directories as $directory) {
            $themes[] = $this->getThemeInfoForPath($directory);
        }
        return view('themestylist::index', compact('themes'));
    }

    /**
     * Get all theme directories
     * @return array
     */
    private function getDirectories()
    {
        $themePath = config('themestylist.themes.paths', [base_path('/Themes')]); // ??how
        return $this->finder->directories($themePath[0]);
    }

    /**
     * @param string $directory
     * @return Theme
     */
    public function getThemeInfoForPath($directory)
    {
        $themeJson = new Json($directory);

        $theme = new Theme(
            $themeJson->getJsonAttribute('name'),
            $themeJson->getJsonAttribute('description'),
            $directory,
            $themeJson->getJsonAttribute('parent')
        );
        $theme->version = $themeJson->getJsonAttribute('version');
        $theme->type = ucfirst($themeJson->getJsonAttribute('type'));
        $theme->changelog = $this->getChangelog($directory);
        $theme->active = $this->getStatus($theme);

        return $theme;
    }

    /**
     * Check if the theme is active based on its type
     * @param Theme $theme
     * @return bool
     */
    private function getStatus(Theme $theme)
    {
        if ($theme->type !== 'Backend') {
            return config('themestylist.frontend') === $theme->getName();
        }

        return  config('themestylist.backend') === $theme->getName();
    }

    /**
     * Show the specified resource.
     * @param Request $request
     * @return Response
     */
    public function show(Request $request)
    {
        $directories = $this->getDirectories();
        foreach ($directories as $directory) {
            $theme = $this->getThemeInfoForPath($directory);
            if($theme->getName() === $request->theme)
            {
                return view('themestylist::show', compact('theme'));
            }
        }
    }

    /**
     * @param string $directory
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function getChangelog($directory)
    {
        if (! $this->finder->isFile($directory . '/changelog.yml')) {
            return [];
        }

        $yamlFile = $this->finder->get($directory . '/changelog.yml');

        $yamlParser = new Parser();

        $changelog = $yamlParser->parse($yamlFile);

        $changelog['versions'] = $this->limitLastVersionsAmount(array_get($changelog, 'versions', []));

        return $changelog;
    }

    /**
     * Publish assets
     * @param Theme $theme
     * @return artisan
     */
    public function publishAssets(Request $request)
    {
        try {
            Artisan::call('stylist:publish', ['theme' => $request->theme]);
        } catch (InvalidArgumentException $e) {
        }
    }

    /**
     * Limit the versions to the last 5
     * @param array $versions
     * @return array
     */
    private function limitLastVersionsAmount(array $versions)
    {
        return array_slice($versions, 0, 5);
    }



    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //dd($request->type);
        $type = Str::lower($request->type);
        $config = config("themestylist.$type");
        $content = file_get_contents(base_path('Config')."/themestylist.php");
        $content = str_replace($config, $request->theme, $content);
        file_put_contents(base_path('Config')."/themestylist.php", $content);

        \Artisan::call('cache:clear');
        \Artisan::call('config:clear');

        return Redirect::route('listthemes');


    }



    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('themestylist::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
